export class Project {
    Project_ID:number;
    Project_Name:string;
    Priority:number;    
    Start_Date:Date;
    End_Date:Date;
    Manager_ID:number;
    Completed_Task:number = 0;
}
