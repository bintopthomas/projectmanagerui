export class Task {
        
    Task_ID:number;
    Project_ID:number;
    Parent_ID:number;    
    Task_Name:string;
    Priority:number;
    Start_Date:Date;   
    End_Date:Date;
    Status:number;
    TASK_OWNER_ID:number;

}
