import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/SERVICES/service.service';
import { Task } from 'src/app/models/task';
import { Parenttask } from 'src/app/MODELS/parenttask';
import { Router } from '@angular/router';
import * as _ from 'lodash';
@Component({
  selector: 'app-view-task',
  templateUrl: './view-task.component.html',
  styleUrls: ['./view-task.component.css']
})
export class ViewTaskComponent implements OnInit {
  sortedOrder: string;
  sortedBy: string;

  constructor(private apiservice:ServiceService,private router: Router) { }
  tasksdata:  Array<Task>;
  parentTaskData: Array<Parenttask>;
  parentTaskDetails:any;
  projectdisplay:any = 'none';
  projectsdata:any;
  searchProject:any= '';
  searchProjectID:number;
  searchtext:any;
  projectcolumns : Array<string> = ["Project_Name", "Priority", "StartDate", "EndDate"];
  ngOnInit() {
    this.apiservice.getAllTask().subscribe(response=>
      {

        console.log("response",response);
        this.tasksdata = response;

        // console.log(JSON.stringify(this.tasksdata.Task_ID));
        // this.tasksdata.Start_Date = this.tasksdata.Start_Date.substr(0,10);
        // this.tasksdata.End_Date = this.tasksdata.End_Date.substr(0,10);
      } );
      this.apiservice.getParenTaskData().subscribe(response=>
        {
    
          console.log("response",response);
          this.parentTaskData = response;
        } );
     
  }
  getParentTask(parentid)
  {
    if(parentid != null)
    {
    this.parentTaskDetails = this.parentTaskData.find(p=>p.Parent_ID == parentid);
    return this.parentTaskDetails.Parent_Task;
    }
    else
    {
      return null;
    }
  }
  OpenprojectModalDialog()
  {

    this.projectdisplay='block';
    this.apiservice.getProjectData().subscribe(response=>
      {
 
        console.log("response",response);
        this.projectsdata = response;
      } );
      this.searchProject = '';
      this.searchProjectID = undefined;
  }
  closeProjectModalDialog()
  {

    this.projectdisplay='none';
  }
  select(project)
  {
  
 this.searchProject = project.Project_Name;
 this.searchProjectID = project.Project_ID;
 console.log('selct' + project.Project_ID);
 this.projectdisplay='none';
  }
  getStartDate(startdate)
  {

    if(startdate != null)
    return startdate.substr(0,10);
    else    
    return startdate;
  }
  getEndDate(enddate)
  {    
    if(enddate != null)
    return enddate.substr(0,10);
    else    
    return enddate;
  }
  redirectPage(task){
    
    this.router.navigate(['add-task/'+ task.Task_ID] );
    //this.router.navigateByUrl('/update/');
     
   }
  endTask(task)
  {
    
    this.apiservice.putEndTask(task).subscribe(response=>
      {
 
        console.log("response",response);
        this.tasksdata = response;
      } );
  }
  deleteTask(task)
  {

    this.apiservice.deleteTaskData(task).subscribe(response=>
      {
 
        console.log("response",response);
        this.tasksdata = response;
      } );
  }
  sortTasks(sortBy: string) {
    //console.log(this.projectsdata);
    
   var sortOrder = this.sortedBy != sortBy || this.sortedOrder == "desc" ? "asc" : "desc";
   this.tasksdata = _.orderBy(this.tasksdata, [sortBy], [sortOrder]);
   this.sortedBy = sortBy;
   this.sortedOrder = sortOrder;
  }
}
