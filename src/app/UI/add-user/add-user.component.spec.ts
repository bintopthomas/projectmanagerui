import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUserComponent } from './add-user.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import {Pipe, PipeTransform} from '@angular/core';
import { User } from 'src/app/MODELS/user';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';

@Pipe({
  name: 'userfilterPipe'
})
export class FilterPipePipe implements PipeTransform {

  transform(users: User[], searchTerm:string): User[] {
    if(!users || (!searchTerm))
    {
      

      return users;
    }
    else    
    {
      return users.filter(users=>
        users.EmployeeId.toString() === searchTerm
      || users.FirstName.toString().toLowerCase() === searchTerm.toLowerCase()
      || users.LastName.toString().toLowerCase() === searchTerm.toLowerCase()
     
      );
    }
  }

}

describe('AddUserComponent', () => {
  let component: AddUserComponent;
  let fixture: ComponentFixture<AddUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],

      imports: [ReactiveFormsModule, HttpClientModule, RouterTestingModule,FormsModule ],
      declarations: [ AddUserComponent,FilterPipePipe ]
    })
    .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserComponent);
    component = fixture.componentInstance;
    
    fixture.detectChanges();

    spyOn(component, 'reset')
  });

  // it('columns of users',()=>{
    
  //   let count;
  //   count = component.columns.length;
  //   console.log('count of users' + count);
  //   ;
    
  // });

  it('should reset',()=>{
    
    component.reset();
    console.log("User details" + component.userDetails);
    expect(component.userDetails).toBeDefined();
    
  });
  it('columns length',()=>{
    
    console.log('spec' + component.columns);
    component.reset();
    expect(component.columns.length).toEqual(3);
    
  });
  it('user edit',()=>{
    
    console.log('spec' + component.columns);
    component.usersdata = 
    [{

      User_ID:1, FirstName: 'Roshan', LastName: 'Jagadish', EmployeeId: 1234
    }]
    component.edit({User_ID:1, FirstName: 'Roshan', LastName: 'Jagadish', EmployeeId: 1234 });
    expect(component.edited).toBeTruthy();
    
  });
  it('delete user',()=>{
    
    console.log('spec' + component.columns);
    component.usersdata = 
    [{

      User_ID:1, FirstName: 'Roshan', LastName: 'Jagadish', EmployeeId: 1234
    }]
    component.delete({User_ID:1, FirstName: 'Roshan', LastName: 'Jagadish', EmployeeId: 1234 });
    expect(component.edited).toBeFalsy();
    
  });
  it('sort user by FirstName',()=>
  {
    
    component.usersdata = 
    [{

      User_ID:1, FirstName: 'Roshan', LastName: 'Jagadish', EmployeeId: 1234
    }]
   
    component.sortUsers('FirstName');
    expect(component.sortedOrder).toEqual('asc');
   
    
  });
  
  
})


