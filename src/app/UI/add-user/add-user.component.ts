import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/MODELS/user';
import { ServiceService } from 'src/app/SERVICES/service.service';
import { NgModule } from '@angular/core';
import { OrderModule } from 'ngx-order-pipe';
import * as _ from 'lodash';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  sortDirection: number = 1;
  sortKey: any;
  searchtext: string;
  usersdata: Array<any>;
  displayaddbutton: any = 'block';
  displayupdatebutton: any = 'none';
  edited: boolean = false;
  columns: Array<string> = ["FirstName", "LastName", "EmployeeId"];

  userDetails: any = {};
  users: User[];
  sortedBy;
  sortedOrder;


  // userDetails:User =
  //   {
  //     User_ID:null,
  //     FirstName:null,
  //     LastName:null,
  //     EmployeeId:null,
  //     Project_ID:null,
  //     Task_ID:null

  //   };

  constructor(private apiservice: ServiceService) { }

  ngOnInit() {
    this.sortKey = 'FirstName';
    this.apiservice.getUserData().subscribe(response => {

      console.log("response", response);
      this.usersdata = response;
    });


    console.log("this.columns", this.columns);
  }
  reset() {
    this.userDetails = {};
    this.edited = false;
  }
  addUser() {



    console.log(this.userDetails);
    this.apiservice.postUserData(this.userDetails)
      //.subscribe(i=>this.msg = i);
      .subscribe(response => {
        this.usersdata = response;
        console.log(response);

        // alert('User Added!')

      });

    this.reset();
  }
  UpdateUser() {
    console.log(this.userDetails);
    this.apiservice.putUserData(this.userDetails)
      //.subscribe(i=>this.msg = i);
      .subscribe(response => {
        //this.usersdata = response;
        console.log(response);
        this.apiservice.getUserData().subscribe(response => {

          console.log("response", response);
          this.usersdata = response;
        });
        // alert('User Added!')

      });



    this.reset();
  }

  edit(user) {

    this.userDetails = { ...this.usersdata.find(p => p.User_ID == user.User_ID) };
    this.edited = true;
    //this.router.navigateByUrl('/update/');
    // this.characters =  this.characters.find(p=> p.TaskId == this.TaskId);
  }
  delete(user) {

    console.log(user);
    this.apiservice.deleteUserData(user)
      //.subscribe(i=>this.msg = i);
      .subscribe(response => {

        this.usersdata = response;

      });
  }
  sortUsers(sortBy: string) {
    console.log(this.usersdata);
    var sortOrder = this.sortedBy != sortBy || this.sortedOrder == "desc" ? "asc" : "desc";
    this.usersdata = _.orderBy(this.usersdata, [sortBy], [sortOrder]);
    this.sortedBy = sortBy;
    this.sortedOrder = sortOrder;
  }





}
