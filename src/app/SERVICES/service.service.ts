import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';

import {Task} from 'src/app/models/task';
import {User} from 'src/app/models/user';
import { Observable } from 'rxjs';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import { stringify } from '@angular/core/src/util';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http:HttpClient) { }

  getUserColumns(){
    return ["FirstName", "LastName", "EmployeeId", "Project_ID", "Task_ID"]};

  postUserData(UserDetail:any):Observable<any>
   {
    console.log('service' + JSON.stringify(UserDetail));  
    return this.http.post("http://localhost/ProjectManagerWebAPI/PostUser/",
    UserDetail).map(res => res );   
    // return this.http.post("http://localhost:53587/PostUser/",
    // UserDetail).map(res => res );   
    
   }
   putUserData(UserDetail:any):Observable<any>
   {
    
    return this.http.put("http://localhost/ProjectManagerWebAPI/UpdateUser/",
    UserDetail) 
    .map(res => res );
    //.map((response:Response)=><any>response.json());
  
   }
   putProjectData(ProjectDetail:any):Observable<any>
   {
    
    return this.http.put("http://localhost/ProjectManagerWebAPI/UpdateProject/",
    ProjectDetail) 
    .map(res => res );
    //.map((response:Response)=><any>response.json());
  
   }
   putEndTask(TaskDetail:any):Observable<any>
   {
    
    return this.http.put("http://localhost/ProjectManagerWebAPI/EndTask/",
    TaskDetail) 
    .map(res => res );
    //.map((response:Response)=><any>response.json());
  
   }
   postTaskData(TaskDetail:any):Observable<any>
   {
    console.log('service' + JSON.stringify(TaskDetail));  
    return this.http.post("http://localhost/ProjectManagerWebAPI/PostTask/",
    TaskDetail).map(res => res );   
  
   }
   putTaskData(TaskDetail:any):Observable<any>
   {
    console.log('service' + JSON.stringify(TaskDetail));  
    return this.http.put("http://localhost/ProjectManagerWebAPI/UpdateTask/",
    TaskDetail).map(res => res );   
  
   }
   
   postParentTask(ParentTaskDetail:any):Observable<any>
   {
    console.log('service' + JSON.stringify(ParentTaskDetail));  
    return this.http.post("http://localhost/ProjectManagerWebAPI/PostParentTask",
    ParentTaskDetail).map(res => res );  
   }
   postProject(ProjectDetail:any):Observable<any>
   {
    console.log('service' + JSON.stringify(ProjectDetail));  
    return this.http.post("http://localhost/ProjectManagerWebAPI/PostProject/",
    ProjectDetail).map(res => res );  
   }
   

   getUserData():Observable<any>
   {
    return this.http.get("http://localhost/ProjectManagerWebAPI/GetAllUser");
 
   }
   getAllTask():Observable<any>
   {
    return this.http.get("http://localhost/ProjectManagerWebAPI/GetAllTask");
 
   }
   getByTaskId(task_ID_UPD:any):Observable<any>
   {
    return this.http.get("http://localhost/ProjectManagerWebAPI/getByTaskId?TaskId=" + task_ID_UPD);   
 
   }
   
   
   getProjectData():Observable<any>
   {
    return this.http.get("http://localhost/ProjectManagerWebAPI/GetAllProject");
 
   }
   getParenTaskData():Observable<any>
   {
    return this.http.get("http://localhost/ProjectManagerWebAPI/GetAllParentTask");
 
   }
   deleteUserData(UserDetail:any):Observable<any>
   {
    console.log('INSIDE SERVICE' + JSON.stringify(UserDetail) );
    console.log(JSON.stringify(UserDetail.User_ID));  
    return this.http.delete("http://localhost/ProjectManagerWebAPI/DeleteUser?id=" + JSON.stringify(UserDetail.User_ID)
    
    
    ) 
    .map(res => res );
  }
  deleteTaskData(TaskDetail:any):Observable<any>
   {
    console.log('INSIDE SERVICE' + JSON.stringify(TaskDetail) );
    console.log(JSON.stringify(TaskDetail.Task_ID));  
    return this.http.delete("http://localhost/ProjectManagerWebAPI/DeleteTask?id=" + JSON.stringify(TaskDetail.Task_ID)
    
    
    ) 
    .map(res => res );
  }
  deleteProjectData(ProjectDetail:any):Observable<any>
   {
    console.log('INSIDE SERVICE' + JSON.stringify(ProjectDetail) );    
    return this.http.delete("http://localhost/ProjectManagerWebAPI/DeleteProject?id=" + JSON.stringify(ProjectDetail.Project_ID)
       
    ) 
    .map(res => res );
  }
}
